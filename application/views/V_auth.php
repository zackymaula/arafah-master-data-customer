<?php
     header("Cache-Control: private, max-age=10800, pre-check=10800");
     header("Pragma: private");
     header("Expires: " . date(DATE_RFC822,strtotime("+2 day")));
?>

<!doctype html>
<html lang="en" class="fullscreen-bg">
	<head>
		<title>Arafah Electronics & Furnituree</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<!-- VENDOR CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/vendor/themify-icons/css/themify-icons.css">
		<!-- MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/main.css">
		<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/template/assets/css/demo.css">
		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
		<!-- ICONS -->
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/template/assets/img/apple-icon.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/icon-arafahelectronics.png">
	</head>
	<body>
		<!-- WRAPPER -->
		<div id="wrapper">
			<div class="vertical-align-wrap">
				<div class="vertical-align-middle">
					<div class="auth-box ">
						<div class="left">
							<div class="content">
								<div class="header">

                  <?php
                    if ($this->session->flashdata('auth_code') == '1') {
                      echo '
                      <div class="alert alert-success alert-dismissible" role="alert">
    										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    											<span aria-hidden="true">&times;</span>
    										</button>
    										<i class="fa fa-check-circle"></i> Selamat, Anda sukses login !
    									</div>';
                    } else if ($this->session->flashdata('auth_code') == '0')  {
                      echo '
                      <div class="alert alert-danger alert-dismissible" role="alert">
    										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    											<span aria-hidden="true">&times;</span>
    										</button>
    										<i class="fa fa-warning"></i> Maaf, username atau password Anda salah.<br>Mohon ulangi lagi !
    									</div>';
                    }
                  ?>


                	<div class="logo text-center">
										<img src="<?php echo base_url(); ?>assets/images/logo-arafahelectronics.png" alt="Arafah Logo">
									</div>
									<p class="lead">Login to your account</p>
								</div>
								<form class="form-auth-small" action="<?php echo base_url(); ?>authlogin" method="POST">
									<div class="form-group">
										<label for="signin-usernm" class="control-label sr-only">Username</label>
										<input type="text" class="form-control" name="post_usernm" id="signin-usernm" placeholder="Username">
									</div>
									<div class="form-group">
										<label for="signin-password" class="control-label sr-only">Password</label>
										<input type="password" class="form-control" name="post_passwd"  id="signin-password" placeholder="Password">
									</div>
									<div class="form-group clearfix">
										<label class="fancy-checkbox element-left custom-bgcolor-blue">
											<input type="checkbox">
											<span class="text-muted">Remember me</span>
										</label>
									</div>
									<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								</form>
							</div>
						</div>
						<div class="right">
							<div class="overlay"></div>
							<div class="content text">
								<h1 class="heading">Arafah Electronics & Furniture</h1>
								<p>by Developer</p>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- END WRAPPER -->
		<!-- Javascript -->
		<script src="<?php echo base_url(); ?>assets/template/assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template/assets/vendor/pace/pace.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/template/assets/scripts/klorofilpro-common.js"></script>
		<!-- END DEMO PANEL -->
	</body>
</html>
