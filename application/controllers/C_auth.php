<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_auth extends CI_Controller {

	public function index()
	{
		$this->load->view('V_auth');
	}

	public function proses_login()
	{
		$usernm = $_POST['post_usernm'];
		$passwd = $_POST['post_passwd'];

		$dataUsers = $this->m_auth->GetUsers($usernm,$passwd);
		$arrayDataUsers = $dataUsers->result_array();

		if ($dataUsers->num_rows()>=1) {
			foreach ($arrayDataUsers as $data) {
				$user_id = $data['id_users'];
				$user_role = $data['id_role'];
				$user_name = $data['user_name'];
			}
			$this->session->set_userdata('user_id', $user_id);
			$this->session->set_userdata('user_role', $user_role);
			$this->session->set_userdata('user_name', $user_name);

			$this->session->set_flashdata('auth_code', '1');
			redirect('auth');
			//echo "login success";
		} else {

			$this->session->set_flashdata('auth_code', '0');
			redirect('auth');
		}
	}

	public function proses_logout()
	{
		$this->session->sess_destroy();
		redirect('auth');
	}
}
