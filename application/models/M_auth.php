<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {

  public function GetUsers($usernm,$passwd)
	{
		$arrayWhere = array(
			'user_usernm' => $usernm,
			'user_passwd' => $passwd,
		);

		$this->db->select('*');
		$this->db->from('u_users');
		$this->db->where($arrayWhere);
		$query = $this->db->get();
		return $query;
	}

}
